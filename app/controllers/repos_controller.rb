class ReposController < ApplicationController
  def index
	@handle = params[:handle]
	@repos = Github.getRepos(@handle)
	response.headers["Content_Type"] = 'application/json'
	render json: JSON.pretty_generate(@repos)
  end

  def error
	render nothing: true, status: 404, content_type: "application/json"	
  end
end
