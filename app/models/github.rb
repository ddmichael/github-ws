class Github
	include HTTParty
	base_uri 'https://api.github.com/users'	
	default_params :output => 'json'
	
	def self.getRepos(handle)
		response = get("/#{handle}/repos")
		if response.success?
			response.sort_by { |v| v["size"] }.reverse!.first 5
		else	
			response = {"message" => "Not found."}	
		end
	end
end
