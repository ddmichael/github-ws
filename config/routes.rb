Rails.application.routes.draw do
  get '/repos/:handle' => 'repos#index'
  get '*path' => 'repos#error'
end
